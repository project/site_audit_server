<?php

namespace Drupal\site_audit_server\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Site Audit Remote Server settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'site_audit_server_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['site_audit_server.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['site_entity_create'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Create a site entity when receiving reports.'),
      '#default_value' => $this->config('site_audit_server.settings')->get('site_entity_create'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
//    if ($form_state->getValue('allowed_clients') != 'allowed_clients') {
//      $form_state->setErrorByName('example', $this->t('The value is not correct.'));
//    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('site_audit_server.settings')
      ->set('site_entity_create', $form_state->getValue('site_entity_create'))
      ->save();
    parent::submitForm($form, $form_state);
  }
}
