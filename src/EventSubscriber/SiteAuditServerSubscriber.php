<?php

namespace Drupal\site_audit_server\EventSubscriber;

use Drupal\site_audit_server\Event\SiteAuditReceivedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Site Audit Server event subscriber.
 */
class SiteAuditServerSubscriber implements EventSubscriberInterface {

  /**
   * Kernel request event handler.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   Response event.
   */
  public function onKernelRequest(RequestEvent $event) {
    // @todo Place code here.
  }

  /**
   * Report Received event
   *
   * @param SiteAuditReceivedEvent $event
   *   Response event.
   */
  public function onReportReceived(SiteAuditReceivedEvent $event) {
    // @todo: If site_entity_create is true, create the entity.

  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      SiteAuditReceivedEvent::REPORT_RECEIVED => ['onReportReceived'],
    ];
  }

}
