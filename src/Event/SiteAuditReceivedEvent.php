<?php
/**
 * @file
 *
 * Helped by https://www.drupal.org/docs/creating-modules/subscribe-to-and-dispatch-events#s-my-first-drupal-8-event-and-event-dispatch
 */
namespace Drupal\site_audit_server\Event;

use Drupal\site_audit_report_entity\Entity\SiteAuditReport;
use Drupal\Component\EventDispatcher\Event;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;

/**
 * Event that is fired when a report is sent.
 */
class SiteAuditReceivedEvent extends Event {

  const REPORT_RECEIVED = 'site_audit_report_received';

  public $report;
  public $request;
  public $response;

  /**
   * Constructs the object.
   *
   * @param SiteAuditReport $report
   *   The report that was just received.
   *
   * @param Response $response
   *   The response to be sent to the reporter.
   *
   */
  public function __construct(SiteAuditReport $report, Request $request, Response $response) {
    $this->report = $report;
    $this->request = $request;
    $this->response = $response;
  }
}
